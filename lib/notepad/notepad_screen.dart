import 'package:flutter/material.dart';

var txt = TextEditingController();

class NotepadScreen extends StatefulWidget {
  final Function(String, bool) addTextToChatLog;

  NotepadScreen({Key? key, required this.addTextToChatLog}) : super(key: key);

  @override
  NotepadScreenState createState() => NotepadScreenState();
}

class NotepadScreenState extends State<NotepadScreen> {
  final TextEditingController _textEditingController = TextEditingController();

  void _clearText() {
    final text = _textEditingController.text;
    widget.addTextToChatLog(text, false);
    _textEditingController.clear();
  }

  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        extendBody: true,
        body: Stack(children: [
          Container(
              //color: Colors.black,
              alignment: Alignment.bottomCenter,
              height: 900.0,
              child: Column(
                children: [
                  Expanded(
                      child: TextField(
                    decoration: const InputDecoration.collapsed(
                      hintText: 'Type your Text here:',
                      hintStyle: TextStyle(
                          color: Color.fromARGB(204, 0, 0, 0),
                          fontWeight: FontWeight.bold),
                    ),
                    controller: _textEditingController,
                    maxLines: null,
                    expands: true,
                    textAlign: TextAlign.center,
                    textAlignVertical: TextAlignVertical.center,
                    keyboardType: TextInputType.name,
                    style: const TextStyle(
                        color: Color.fromARGB(255, 0, 0, 0),
                        fontWeight: FontWeight.bold),
                  ))
                ],
              )),
          Container(
              //color: Colors.black,
              alignment: Alignment.bottomCenter,
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CircleAvatar(
                      radius: 30,
                      backgroundColor: Color.fromARGB(255, 0, 0, 0),
                      child: IconButton(
                        icon: Icon(Icons.clear),
                        color: const Color.fromARGB(255, 255, 255, 255),
                        iconSize: 40,
                        onPressed: _clearText,
                      )))),
        ]));
  }
}
