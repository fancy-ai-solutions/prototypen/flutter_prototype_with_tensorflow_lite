import 'package:flutter/material.dart';
import 'package:flutter_prototype_with_tensorflow_lite/camera/camera_screen.dart';
import 'package:flutter_prototype_with_tensorflow_lite/chatlog/chatlog_screen.dart';
import 'package:flutter_prototype_with_tensorflow_lite/global_bindings.dart';
import 'package:flutter_prototype_with_tensorflow_lite/notepad/notepad_screen.dart';
import 'package:get/get.dart';

void main() {
  runApp(FancyAIApp());
}

class FancyAIApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return GetMaterialApp(
        initialBinding: GlobalBindings(),
        debugShowCheckedModeBanner: false,
        title: "Camera Application",
        defaultTransition: Transition.fade,
        transitionDuration: Duration(seconds: 2),
        home: NavigationPage());
  }
}

class NavigationPage extends StatefulWidget {
  @override
  NavigationPageState createState() => NavigationPageState();
}

class NavigationPageState extends State<NavigationPage> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
  final List<Widget> _children = [];
  final GlobalKey<ChatScreenState> _chatLogPageKey =
      GlobalKey<ChatScreenState>();

  void addTextToChatLog(String text, bool isMe) {
    final chatLogPage = _chatLogPageKey.currentState;
    chatLogPage?.addMessage(text, isMe);
  }

  late final screens = [
    CameraScreen(addTextToChatLog: addTextToChatLog),
    NotepadScreen(addTextToChatLog: addTextToChatLog),
    ChatScreen(key: _chatLogPageKey)
  ];

  @override
  void initState() {
    super.initState();
  }

  NavController bottomNavController = Get.put(NavController());

  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Obx(
        () => IndexedStack(
          index: bottomNavController.selectedIndex.value,
          children: screens,
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.black,
        currentIndex: bottomNavController.selectedIndex.value,
        onTap: (index) {
          bottomNavController.changeIndex(index);
        },
        items: const [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.camera,
              size: 50,
              color: Color.fromARGB(
                216,
                255,
                255,
                255,
              ),
            ),
            label: 'Camera',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.edit,
              size: 50,
              color: Color.fromARGB(
                216,
                255,
                255,
                255,
              ),
            ),
            label: 'Write',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.chat,
              size: 50,
              color: Color.fromARGB(
                216,
                255,
                255,
                255,
              ),
            ),
            label: 'Chatlog',
          ),
        ],
        selectedLabelStyle: const TextStyle(
          fontWeight: FontWeight.w400,
          color: Color.fromARGB(
            216,
            255,
            255,
            255,
          ),
        ),
        unselectedLabelStyle: const TextStyle(
          //fontSize: 16,
          fontWeight: FontWeight.w400,
          color: Color.fromARGB(
            216,
            255,
            255,
            255,
          ),
        ),
        showSelectedLabels: true,
        showUnselectedLabels: true,
      ),
    );
  }
}

class NavController extends GetxController {
  var selectedIndex = 0.obs;

  void changeIndex(int index) {
    selectedIndex.value = index;
  }
}
