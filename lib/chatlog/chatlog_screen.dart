import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChatMessage {
  final String text;
  final bool isMe;

  ChatMessage({required this.text, required this.isMe});
}

class ChatlogController extends GetxController {
  final List<ChatMessage> _messages = [];

  void addMessage(String text, bool isMe) {
    if (text != "") _messages.add(ChatMessage(text: text, isMe: isMe));
  }
}

class ChatScreen extends StatefulWidget {
  ChatScreen({Key? key}) : super(key: key);
  final GlobalKey<ChatScreenState> chatScreenStateKey =
      GlobalKey<ChatScreenState>();

  @override
  ChatScreenState createState() => ChatScreenState();
}

class ChatScreenState extends State<ChatScreen> {
  final GlobalKey<ChatScreenState> chatScreenStateKey =
      GlobalKey<ChatScreenState>();
  final List<ChatMessage> _messages = [];
  late ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void addMessage(String text, bool isMe) {
    if (text != "") {
      setState(() {
        _messages.add(ChatMessage(text: text, isMe: isMe));
      });

      WidgetsBinding.instance!.addPostFrameCallback((_) {
        _scrollController.animateTo(
          _scrollController.position.maxScrollExtent,
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut,
        );
      });
    }
  }

  Widget _buildMessage(ChatMessage message) {
    final messageWidget = Container(
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 16.0, left: 16),
            alignment: Alignment.centerRight,
            child: CircleAvatar(
              backgroundColor: Color.fromARGB(message.isMe ? 255 : 0, 0, 0, 0),
              foregroundColor: const Color.fromARGB(255, 255, 255, 255),
              child: Text(message.isMe ? 'C' : 'N'),
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  alignment: message.isMe
                      ? Alignment.centerLeft
                      : Alignment.centerRight,
                  padding: const EdgeInsets.only(right: 10),
                  child: Text(
                    message.isMe ? 'Cam' : 'Note',
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  alignment: message.isMe
                      ? Alignment.centerLeft
                      : Alignment.centerRight,
                  margin: const EdgeInsets.only(top: 5.0),
                  padding: const EdgeInsets.only(right: 10),
                  child: Text(message.text),
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(right: 16.0),
            alignment: Alignment.centerRight,
            child: CircleAvatar(
              backgroundColor: Color.fromARGB(message.isMe ? 0 : 255, 0, 0, 0),
              foregroundColor: const Color.fromARGB(255, 255, 255, 255),
              child: Text(message.isMe ? 'C' : 'N'),
            ),
          )
        ],
      ),
    );

    if (message.isMe) {
      return messageWidget;
    }

    return GestureDetector(
      child: messageWidget,
      onTap: () {
        // Perform action when other user's message is tapped
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    //addMessage("Test Text", true);
    return Scaffold(
      key: chatScreenStateKey,
      body: Column(
        children: [
          Padding(padding: EdgeInsets.only(top: 50.0)),
          Expanded(
            child: ListView.builder(
              controller: _scrollController,
              padding: EdgeInsets.all(8.0),
              reverse: false,
              itemCount: _messages.length,
              itemBuilder: (BuildContext context, int index) {
                final message = _messages[index];
                return _buildMessage(message);
              },
            ),
          ),
        ],
      ),
    );
  }
}
