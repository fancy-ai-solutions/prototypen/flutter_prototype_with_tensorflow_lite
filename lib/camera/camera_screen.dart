import 'package:flutter/material.dart';
import 'package:flutter_prototype_with_tensorflow_lite/camera/camera_viewer.dart';
import 'package:flutter_prototype_with_tensorflow_lite/camera/translated_text.dart';

class CameraScreen extends StatelessWidget {
  final Function(String, bool) addTextToChatLog;

  const CameraScreen({Key? key, required this.addTextToChatLog})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(alignment: Alignment.bottomCenter, children: [
      const CameraViewer(),
      Column(
        children: [
          const Spacer(),
          TranslatedText(addTextToChatLog: addTextToChatLog),
        ],
      ),
    ]));
  }
}
