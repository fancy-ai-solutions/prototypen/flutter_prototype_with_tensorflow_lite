import 'package:flutter/material.dart';

var txt = TextEditingController();

class TranslatedText extends StatefulWidget {
  const TranslatedText({Key? key, required this.addTextToChatLog})
      : super(key: key);
  final Function(String, bool) addTextToChatLog;

  @override
  TranslatedTextState createState() => TranslatedTextState();

  setTextFromNN(text) {
    txt.text = text;
  }
}

class TranslatedTextState extends State<TranslatedText> {
  String _prevString = "";

  @override
  void initState() {
    super.initState();
    txt.addListener(_addLetterToChatLog);
  }

  @override
  void dispose() {
    txt.removeListener(_addLetterToChatLog);
    txt.dispose();
    super.dispose();
  }

  void _addLetterToChatLog() {
    if (txt.text != "") {
      String letter = txt.text.split(" ").first;
      if (!(letter == _prevString)) {
        widget.addTextToChatLog(letter, true);
        _prevString = letter;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    txt.text = "Test";
    return Stack(alignment: Alignment.bottomCenter, children: [
      Container(
          // color: const Color.fromARGB(131, 0, 0, 0),
          height: 180,
          //child: Expanded(
          child: Padding(
            padding: const EdgeInsets.only(top: 40),
            child: TextField(
              controller: txt,
              expands: true,
              maxLines: null,
              readOnly: true,
              textAlign: TextAlign.center,
              textAlignVertical: TextAlignVertical.top,
              style: const TextStyle(
                  color: Color.fromARGB(255, 255, 255, 255),
                  //fontSize: 32,
                  fontWeight: FontWeight.bold),
            ),
          )),
      Padding(
          padding: const EdgeInsets.all(8.0),
          child: CircleAvatar(
              radius: 30,
              backgroundColor: const Color.fromARGB(255, 255, 255, 255),
              child: IconButton(
                icon: Icon(Icons.clear),
                color: const Color.fromARGB(255, 0, 0, 0),
                iconSize: 40,
                onPressed: clearText,
              ))),
    ]);
  }

  clearText() {
    final text = txt.text;
    widget.addTextToChatLog(text, true);
    txt.clear();
  }
}
