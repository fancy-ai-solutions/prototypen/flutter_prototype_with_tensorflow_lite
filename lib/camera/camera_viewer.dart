//import 'dart:ffi';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_prototype_with_tensorflow_lite/scan_controller.dart';
import 'package:get/get.dart';

class CameraViewer extends GetView<ScanController> {
  const CameraViewer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = Get.width;
    final height = Get.height;
    return GetX<ScanController>(builder: (controller) {
      if (!controller.isInitialized) {
        return Container();
      }

      return SizedBox(
        height: Get.height,
        width: Get.width,
        child: AspectRatio(
          aspectRatio: controller.cameraController.value.aspectRatio,
          child: Stack(
            children: [
              SizedBox(
                child: CameraPreview(controller.cameraController),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  color: Colors.black,
                  height: (height / 4) - 15,
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  color: Colors.black,
                  height: (height / 4) - 35,
                ),
              ),
            ],
          ),
        ),
      );
    });
  }
}
