import 'dart:io';
import 'dart:typed_data';

import 'package:camera/camera.dart';
import 'package:flutter_prototype_with_tensorflow_lite/camera/translated_text.dart';
import 'package:flutter_tflite/flutter_tflite.dart';
import 'package:get/get.dart';
import 'package:image/image.dart' as img;
import 'package:path_provider/path_provider.dart';

class ScanController extends GetxController {
  late List<CameraDescription> _cameras;
  late CameraController _cameraController;

  final RxBool _isInitialized = RxBool(false);
  final RxList<Uint8List> _imageList = RxList([]);

  int _imageCount = 0;
  bool interpreterBusy = false;

  CameraController get cameraController => _cameraController;

  bool get isInitialized => _isInitialized.value;

  List<Uint8List> get imageList => _imageList;

  @override
  void onInit() {
    initCamera();
    super.onInit();
  }

  @override
  void dispose() {
    _isInitialized.value = false;
    _cameraController.dispose();
    Tflite.close();
    super.dispose();
  }

  Future<void> initCamera() async {
    _cameras = await availableCameras();
    _cameraController = CameraController(
      _cameras[0],
      ResolutionPreset.high,
      imageFormatGroup:
      Platform.isIOS ? ImageFormatGroup.bgra8888 : ImageFormatGroup.yuv420,
    );

    _cameraController.initialize().then((value) {
      _isInitialized.value = true;
      _cameraController.startImageStream((image) {

        _imageCount++;

        // Zähle Bilder aus Stream und wende _objectRecognition auf jedes 120ste Bild an
        if (_imageCount % 120 == 0 && interpreterBusy == false) {
          _imageCount = 0;
          _objectRecognition(image);
        }
      });

      _isInitialized.refresh();
    }).catchError((Object e) {
      if (e is CameraException) {
        switch (e.code) {
          case 'CameraAccessDenied':
            print('User denied camera access.');
            break;
          default:
            print('Handle other errors.');
            break;
        }
      }
    });
  }

  Future<void> _objectRecognition(CameraImage cameraImage) async {

    /*
    Object Recognition Variablen:

    modelFolder: Angabe des KI-Ordners im 'assets'-Ordner
    numberOfModels: Angabe der genutzten Segmente des KI-Modells
    confidenceToMatch: Angabe der Confidence über welcher das Majority Voting erkannte Gesten zulässt
     */

    String modelFolder = 'KI_5_2';
    int numberOfModels = 5;
    double confidenceToMatch = 0.92;

    int modelCounter = 1;
    int nothingCounter = 0;
    List possibleElements = [];

    img.Image data = convertYUV420ToImage(cameraImage); // Konvertiert das Bild der Kamera in das Format der Trainingsdaten (224px x 224px)

    // Zwischenspeichern des Bildes
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    String filePath = '$tempPath/cropped_image.png';
    File file = File(filePath);
    await file.writeAsBytes(img.encodePng(data));

    print("---------------------------------------------");
    while (modelCounter <= numberOfModels) {
      late List recognitionsList;

      interpreterBusy = true;
      recognitionsList = await runModelOnCameraImage(
          file.path, 'Segment_$modelCounter', modelFolder); // Setzt Busy-Flag und führt das TFlite Modell auf dem formatierten Bild aus
      interpreterBusy = false;

      //Majority Voting und Terminal-Ausgabe
      if (recognitionsList.isEmpty) {
        nothingCounter++;
      }
      recognitionsList.forEach((element) {
        print("element from Model Segment_$modelCounter: $element");
        if (element['confidence'] > confidenceToMatch) {
          if (element['label'].toString().split(" ")[1] == 'nichts') {
            nothingCounter++;
          } else {
            possibleElements.add(element);
          }
        } else {
          nothingCounter++;
        }
      });
      modelCounter++;
    }
    print("---------------------------------------------");
    print("$nothingCounter recognitions were below $confidenceToMatch confidence.");
    print("---------------------------------------------");

    if (nothingCounter < numberOfModels) {
      var elementToShow = {};
      double lastConfidence = 0.0;
      possibleElements.forEach((element) {
        if (element['confidence'] > lastConfidence) {
          lastConfidence = element['confidence'];
          elementToShow = element;
        }
      });

      print(
          "Element with highest confidence and above $confidenceToMatch is: $elementToShow");
      if (elementToShow.isNotEmpty) {
        TranslatedText(addTextToChatLog: (p1, p2) => {}).setTextFromNN(
            "${elementToShow['label'].toString().split(" ")[1]} ${(elementToShow['confidence'] * 100).toStringAsFixed(0)}%");
      }
    }
  }

  Future<List> runModelOnCameraImage(
      String filePath, String modelName, String folderName) async {
    await Tflite.loadModel(
        model: "assets/$folderName/$modelName.tflite",
        labels: "assets/$folderName/$modelName.txt",
        numThreads: 1,
        isAsset: true,
        useGpuDelegate:
            false
        );

    List recognitions = (await Tflite.runModelOnImage(
      path: filePath,
      imageMean: 127.5,
      imageStd: 127.5,
      numResults: 5,
      threshold: 0.5,
    ))!;
    Tflite.close();
    return recognitions;
  }

  static img.Image convertYUV420ToImage(CameraImage cameraImage) {
    final width = cameraImage.width;
    final height = cameraImage.height;

    final yRowStride = cameraImage.planes[0].bytesPerRow;
    final uvRowStride = cameraImage.planes[1].bytesPerRow;
    final uvPixelStride = cameraImage.planes[1].bytesPerPixel!;

    final image = img.Image(width, height);

    for (var w = 0; w < width; w++) {
      for (var h = 0; h < height; h++) {
        final uvIndex =
            uvPixelStride * (w / 2).floor() + uvRowStride * (h / 2).floor();
        final index = h * width + w;
        final yIndex = h * yRowStride + w;

        final y = cameraImage.planes[0].bytes[yIndex];
        final u = cameraImage.planes[1].bytes[uvIndex];
        final v = cameraImage.planes[2].bytes[uvIndex];

        image.data[index] = yuv2rgb(y, u, v);
      }
    }

    // Bild auf 1:1 Seitenverhältnis Zuschneiden
    int minSize = width < height ? width : height;
    img.Image croppedImage =
        img.copyCrop(image, minSize ~/ 2, 0, minSize, minSize);

    // Bild auf 224x224 Pixel skalieren
    img.Image resizedImage =
        img.copyResize(croppedImage, width: 224, height: 224);

    return img.copyRotate(resizedImage, 90);
  }

  static int yuv2rgb(int y, int u, int v) {
    var r = (y + v * 1436 / 1024 - 179).round();
    var g = (y - u * 46549 / 131072 + 44 - v * 93604 / 131072 + 91).round();
    var b = (y + u * 1814 / 1024 - 227).round();

    r = r.clamp(0, 255);
    g = g.clamp(0, 255);
    b = b.clamp(0, 255);

    return 0xff000000 |
        ((b << 16) & 0xff0000) |
        ((g << 8) & 0xff00) |
        (r & 0xff);
  }

}
