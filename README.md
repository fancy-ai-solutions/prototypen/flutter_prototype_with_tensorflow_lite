# Flutter Prototyp mit TensorFlow Lite

Dieses Repository enthält einen Flutter-Prototyp, der TensorFlow Lite verwendet, um Gesten zu erkennen. Der Prototyp dient dazu, die Integration von TensorFlow Lite in Flutter-Anwendungen zu demonstrieren und die Leistung von Machine Learning-Modellen auf mobilen Geräten für die Gestenerkennung zu nutzen.

Initial war die Idee eine KI zu trainieren, die alle Zeichen des Alphabets und die Zahlen enthält. Da es aber in der Gebärdensprache viele ähnliche Handzeichen gibt waren wir uns einer Möglichen Problematik über eine Schlechte Erkennung bewusst. Um möglicherweise bessere Ergebnisse unserer KI zu erzielen, sind wir auf die Idee gekommen, ähnliche Handzeichen auf verschiedene KIs zu verteilen. Dadurch muss eine einzelne KI nicht zu viele ähnliche Handzeichen unterscheiden. Wir sind wie folgt vorgegangen: Zuerst haben wir Handzeichen betrachtet, die unmittelbar ähnlich sind, und diese gruppiert. Dadurch entstanden drei KIs (KI_3), von denen jede 12 Handzeichen enthält.

Da wir nicht sicher waren, ob diese Anzahl von KIs ausreichend ist oder möglicherweise schon zu viele, haben wir die ursprüngliche KI (KI_1)mit 36 Handzeichen beibehalten und drei zusätzliche KIs erstellt. Eine in 4 KIs mit je 9 Handzeichen (KI_4), und schließlich wurden zwei KIs in 5 KIs mit je 7 Handzeichen unterteilt (KI_5_1 und KI_5_2). Dabei haben wir auch entferntere Ähnlichkeiten zwischen den Handzeichen berücksichtigt.

## Features

- Integration von TensorFlow Lite in Flutter
- Beispielhafte Anwendung eines Machine Learning-Modells
- Verwendung von Flutter für die Benutzeroberfläche
- Die erkannte Geste wird mit der entsprechenden confidence angezeigt
  -  Es stehen 5 trainierte KI-Modelle für die Einbindung in die App mit unterschiedlicher Segmentierung des in [Verwendung](#verwendung) gezeigten Alphabets zur Verfügung 
      - `KI_1`:
        - `Segment_1`: Gesamtes Alphabet 
      - `KI_3`:
        - `Segment_1`: A,O,N,7,1,B,V,W,5,G,Q,R
        - `Segment_2`: S,0,M,8,Z,F,2,4,6,H,P,U
        - `Segment_3`: 9,D,C,I,J,K,L,T,Y,3,X,E
      - `KI_4`:
        - `Segment_1`: A,O,N,7,1,B,V,5,Q
        - `Segment_2`: S,0,M,8,Z,F,2,4,P
        - `Segment_3`: E,C,T,R,D,H,K,6,L
        - `Segment_4`: Y,X,J,9,I,G,3,W,U
      - `KI_5_1`:
        - `Segment_1`: A,O,N,7,1,B,V,5
        - `Segment_2`: S,0,M,8,Z,F,3
        - `Segment_3`: E,C,T,R,D,H,4
        - `Segment_4`: Y,Q,J,9,L,6,K
        - `Segment_5`: X,P,I,W,U,G,2
      - `KI_5_2`:
        - `Segment_1`: A,O,N,7,1,B,6,K
        - `Segment_2`: S,0,M,8,Z,F,3 
        - `Segment_3`: E,C,T,R,D,H,4 
        - `Segment_4`: Y,Q,J,9,L,V,5 
        - `Segment_5`: X,P,I,G,U,2,W
- Es gibt ein Log in dem das letzte erkannte Zeichen aufgelistet wird

## Voraussetzungen

Um dieses Projekt lokal ausführen und entwickeln zu können, müssen folgende Voraussetzungen erfüllt sein:

- Flutter SDK (Version 3.7.12)
- Dart SDK (Version 2.19.6)
- Android Studio oder Xcode für die Emulation von Android- bzw. iOS-Geräten
- Berechtigung zur Nutzung der Kamera und des Mikrofons
- Folgende Gesten, welche unter Punkt 7 bei verwendung aufzufinden sind

## Installation

1. Stelle sicher, dass ein Emulator läuft, um die Flutter-App auszuführen.

   - Starte einen Android-Emulator in Android Studio oder
   - Starte einen iOS-Emulator in Xcode 

2. Klone dieses Repository auf deinen lokalen Rechner:

    ```bash
    git clone https://gitlab.com/fancy-ai-solutions/prototypen/flutter_prototype_with_tensorflow_lite.git
    ```
   
3. Wechsle in das Projektverzeichnis:

    ```bash
    cd flutter_prototype_with_tensorflow_lite
    ```
   
4. Installiere die Abhängigkeiten, indem du den folgenden Befehl ausführst:

    ```bash
    flutter pub get
    ```

5. Führe die Anwendung auf einem Emulator oder angeschlossenen Gerät aus:

    ```bash
    flutter run
    ```

6. Sobald die App im Emulator geöffnet ist, müssen die erforderlichen Berechtigungen für Kamera und Mikrofon akzeptiert werden. Stelle sicher, dass die App Zugriff auf diese Berechtigungen hat, um die Gebärdenspracherkennung korrekt zu nutzen.

# Verwendung

1. Starte die Anwendung auf deinem Emulator oder Gerät.

2. Die Anwendung verwendet TensorFlow Lite, um ein Machine Learning-Modell auszuführen.

3. Öffne die Datei [main.dart](lib/main.dart), um die Hauptlogik der App zu sehen. Hier findest du den [scan_controller.dart](/lib/scan_controller.dart), der die Kamerafunktionalität und die Gebärdenspracherkennung steuert.

4. Passe bei Bedarf die Logik im [scan_controller.dart](/lib/scan_controller.dart) an, um zusätzliche Funktionen hinzuzufügen oder die Erkennungsleistung zu optimieren. Zum Beispiel:
    - `modelFolder`: Angabe des KI-Ordners im 'assets'-Ordner. Beispiel: `KI_3` für den Ordner `/assets/KI_3/` 
    - `numberOfModels`: Angabe der genutzten Segmente des KI-Modells (Muss mit der Anzahl der Teilmodelle übereinstimmen)
    - `confidenceToMatch`: Angabe der Confidence über welcher das Majority Voting erkannte Gesten zulässt

5. Das Design der App wird ebenfalls in [main.dart](lib/main.dart) definiert. Du kannst das Layout, die Farben und andere visuelle Elemente anpassen, um das Erscheinungsbild der App zu ändern.
 
6. Weitere Informationen zur Verwendung von TensorFlow Lite in Flutter findest du in der offiziellen Dokumentation.

8. Es werden folgende Gesten erkannt:

<picture>
  <img src="cheatsheet.png" width="800">
</picture>

9. Hier ein kleines Beispiel, wie eine erkennung in der App aussieht:

<picture>
  <img src="beispiel.png" width="250">
</picture>

# Beitragende

Wir freuen uns über Beiträge zur Weiterentwicklung dieses Prototyps! Wenn du Verbesserungsvorschläge hast, Fehler gefunden hast oder neue Funktionen hinzufügen möchtest, kannst du uns helfen, indem du Pull Requests einreichst.
